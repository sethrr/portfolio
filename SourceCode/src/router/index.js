import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'
import Index from '@/pages/Index'


Vue.use(Router);
Vue.use(Meta);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Index',
            component: Index,
            meta: {bodyClass: 'index'}
        },
        // {
        //     path: '/not-found',
        //     name: 'NotFound',
        //     component: ErrorPage,
        //     meta: {bodyClass: 'not-found'}
        // },
        // {
        //     path: "*",
        //     redirect: '/not-found'
        // },
        {
            path: "*",
            redirect: '/'
        },
    ]
})
