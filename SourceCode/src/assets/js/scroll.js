
console.log('hello');
window.bullet = window.bullet || {};

window.bullet.initScroll = function () {
    let controller = new ScrollMagic.Controller;

    // .to('@target', @length, {@object})
    let rightTween = TweenMax.to('.hero__bullet-1', 1, {
        right: 0,
        ease: Linear.easeInOut
    });

    // .to('@target', @length, {@object})
    let leftTween = TweenMax.to('.hero__bullet-2', 1, {
        left: 0,
        ease: Linear.easeInOut
    });

    // .to('@target', @length, {@object})
    let copyOutTween = TweenMax.to('.hero__copy-col h2:nth-of-type(1)', 1, {
        opacity: 0,
        ease: Linear.easeInOut
    });

    // .to('@target', @length, {@object})
    let copyInTween = TweenMax.to('.hero__copy-col h2:nth-of-type(2)', 1, {
        opacity: 1,
        ease: Linear.easeInOut
    });

    // .to('@target', @length, {@object})
    let arrowOutTween = TweenMax.to('.hero__arrow--down', 1, {
        opacity: 0,
        ease: Linear.easeInOut
    });

    createScene(150, 500, rightTween);
    createScene(150, 500, leftTween);
    createScene(300, 100, copyOutTween);
    createScene(400, 100, copyInTween);
    createScene(400, 100, arrowOutTween);

    function createScene(offset, duration, tween, trigger = null) {
        new ScrollMagic.Scene({
            triggerElement: trigger,
            duration: duration,
            offset: offset
        }).setTween(tween).addTo(controller);
    }
};

window.bullet.initUnflipper = function () {
    let flipCard = document.querySelector('.poster-tile--disabled.flipped');

    let controller = new ScrollMagic.Controller({
        addIndicators: true
    });

    let flipScene = new ScrollMagic.Scene({
        triggerElement: flipCard,
        triggerHook: 1,
        offset: 200
    })
        .addTo(controller)
        .on("leave", function(){
            if (flipCard.classList.contains('flipped')) {
                flipCard.classList.remove('flipped');
            }
            controller.removeScene(flipScene).removeScene(flipScene2);
        });


    let flipScene2 = new ScrollMagic.Scene({
        triggerElement: flipCard,
        triggerHook: .1,
        offset: 200
    })
        .addTo(controller)
        .on('enter', function(){
            if (flipCard.classList.contains('flipped')) {
                flipCard.classList.remove('flipped');
            }
            controller.removeScene(flipScene).removeScene(flipScene2);
        });
};
