export default getAbsoluteUrl;

function getAbsoluteUrl(context, url, encode) {
    let path = "https://" + process.env.BASE_URL + url;
    return (encode) ? encodeURIComponent(path) : path;
}
