export default {
    "index": {
        "title": "31 Bullets",
        "thumb": "thumbs/index__meta.jpg",
        "body": [
            {"p": "Hello. I am a pretty cool page."},
            {"p": "So, we’re arming people with 31 actions to counter the negative effect that these bullets create."}
        ]
    },
    "readMore": {
        "title": "Read More",
        "img": "editorial-page.jpg",
        "thumb": "thumbs/index__meta.jpg",
        "description": "Read our most recent articles on this important issue",
        "editorialLabel": "<strong>Read our Editorials</strong>",
        "editorialLinks": [
            {"a": "<a href=\"https://chicago.suntimes.com/working/31bullets-campaign-end-gun-violence\" target=\"_blank\">31 bullets: A Chicago Sun-Times campaign to end gun violence</a>"},
            {"a": "<a href=\"https://chicago.suntimes.com/working/editorial-31-bullets-campaign-fight-gun-violence\" target=\"_blank\">We asked how to fix the gun problem. You said we must fix our world.</a>"},
        ],
        "opEdLabel": "<strong>Read our Op-Eds</strong>",
        "opEdLinks": [
            {"a": "<a href=\"https://chicago.suntimes.com/opinion/31-bullets-three-bills-make-illinois-safer-gun-violence-ar-15\" target=\"_blank\">Three bills that would make Illinois safer from gun violence</a>"},
            {"a": "<a href=\"https://chicago.suntimes.com/opinion/bruce-rauner-plays-politics-death-penalty-guns-illinois-governor\" target=\"_blank\">Bruce Rauner plays pure politics with death penalty and guns</a>"},
            {"a": "<a href=\"https://chicago.suntimes.com/opinion/30-years-after-laurie-dann-our-schoolchildren-keep-dying-santa-fe-texas/\" target=\"_blank\">30 years after Laurie Dann, our schoolchildren keep dying</a>"},
            {"a": "<a href=\"https://chicago.suntimes.com/opinion/laurie-dann-survivor-gun-violence-school-shootings-phil-andrew/\" target=\"_blank\">A survivor of Laurie Dann’s deadly day says peace begins with ‘each of us’</a>"},
            {"a": "<a href=\"https://chicago.suntimes.com/opinion/crack-down-shops-sell-guns-crimes-every-day-and-a-half-31-bullets/\" target=\"_blank\">One way to crack down on shops that sell guns used in crimes</a>"},
            {"a": "<a href=\"https://chicago.suntimes.com/opinion/squeeze-gun-makers-curb-gun-violence-profits/\" target=\"_blank\">Putting squeeze on gun-makers could help curb Chicago’s gun violence</a>"},
            {"a": "<a href=\"https://chicago.suntimes.com/opinion/northwestern-university-study-illegal-guns-handshake-gun-violence/\" target=\"_blank\">Getting illegal guns under our lax laws is a simple handshake deal</a>"}
        ],
        "newsLabel": "<strong>News</strong>",
        "newsLinks": [
            {"a": "<a href=\"https://chicago.suntimes.com/news/chicago-students-say-enough-to-gun-violence-through-public-art\" target=\"_blank\">Chicago students say ‘enough’ to gun violence through public art</a>"}
        ],
    },
    "about": {
        "title": "About",
        "img": "bullets/about.jpg",
        "thumb": "thumbs/index__meta.jpg",
        "body": [
            {"p": "There are an estimated 10 billion bullets sold in the US every year. That’s 31 for every man, woman and child."},
            {"p": "At the Chicago Sun-Times, we’re tired of writing headlines about gun violence. So, in order to honor our commitment to being brutally honest, and unflinchingly brave, we’re addressing the issue head on—compiling gun violence solutions and arming people with a different kind of ammunition."},
            {"p": "Knowledge. Education. Action."},
            {"p": "31 bullets to counter the negative effect that these actual bullets create."},
            {"p": "We will be unveiling each of these bullet points through social media, articles, and print—informing and arming you, the reader."},
            {"p": "There isn’t one perfect answer or action to defeat gun violence in America. But by bringing together the most powerful solutions, we can arm people with the tools to combat the problem."}
        ],
        "cite": "Illustrations: Emmanuel Polanco, <a href=\"http://colagene.com\" target=\"_blank\">colagene.com"
    },
    "bullets": {
        "1": {
            "enabledOn": "2018-04-30T06:00:00-05:00",
            "title": "Guns in Classrooms",
            "img": "bullets/bullet-01.jpg",
            "thumb": "thumbs/thumb__bullet-01.jpg",
            "poster": "posters/CST.31Bullets.1of31.Download.24x36.pdf",
            "video": "https://www.youtube.com/embed/DtTNJ5X_CjU?rel=0&amp;controls=0&amp;showinfo=0",
            "body": [
                {"p": "After the Stoneman Douglas High School shooting, President Trump proposed arming teachers with guns as a solution to ending gun violence in schools. We believe this is not only an impractical solution, but a risky one. Instead, we must help our educators recognize the warning signs of potential perpetrators and provide them with the tools for helping those students. Sign the Sandy Hook Promise <a href=\"https://action.sandyhookpromise.org/p/dia/action3/common/public/index.sjs?action_KEY=20091\" target=\"_blank\">petition</a> to keep guns out of our kids' schools."},
                {"p": "Read our first <a href=\"https://chicago.suntimes.com/working/31bullets-campaign-end-gun-violence\" target=\"_blank\">editorial</a> about our campaign to end gun violence."}
            ]
        },
        "2": {
            "enabledOn": "2018-05-07T06:00:00-05:00",
            "title": "Child Lock Requirements",
            "img": "bullets/bullet-02.jpg",
            "thumb": "thumbs/thumb__bullet-02.jpg",
            "poster": "posters/CST.31Bullets.2of31.Download.24x36.pdf",
            "cta": true,
            "body": [
                {"p": "1.7 million children live in homes with loaded and unlocked firearms<sup>1</sup>. The presence of unlocked guns in the home increases the risk of unintentional gun injuries, intentional shootings, and suicides."},
                {"p": "Contact your congresswoman or congressman, as well as your state elected leaders, to promote child-access prevention laws and require firearms to be stored with a locking device in place."}
            ],
            "cite": "<sup>1</sup>New York Times: <a href=\"https://www.nytimes.com/2018/03/09/opinion/guns-children.html/\" target=\"_blank\">https://www.nytimes.com/2018/03/09/opinion/guns-children.html</a>"
        },
        "3": {
            "enabledOn": "2018-05-08T00:00:00-05:00",
            "title": "Concealed Carry",
            "img": "bullets/bullet-03.jpg",
            "thumb": "thumbs/thumb__bullet-03.jpg",
            "tags": "HTML, CSS, Javascript, Vue",
            "poster": "posters/CST.31Bullets.3of31.Download.24x36.pdf",
            "body": [
                {"p": "Concealed carry legislation (H.R. 38) would allow armed individuals, crossing state lines, to abide only by their home state’s concealed carry laws<sup>2</sup>. This would force states with stringent standards, such as Illinois, to recognize other, sometimes laxer, standards."},
                {"p": "<a href=\"https://www.csgv.org/actions/\" target=\"_blank\">Click here</a>, and Tweet your senator to Vote NO on #hr38"}
            ],
            "cite": "<sup>2</sup>Congress: <a href=\"https://www.congress.gov/bill/115th-congress/house-bill/38\" target=\"_blank\">https://www.congress.gov/bill/115th-congress/house-bill/38</a>"
        },
        "4": {
            "enabledOn": "2018-05-09T00:00:00-05:00",
            "title": "Gun Silencers Measure",
            "img": "bullets/bullet-04.jpg",
            "thumb": "thumbs/thumb__bullet-04.jpg",
            "tags": "HTML, CSS, Javascript, Vue",
            "poster": "posters/CST.31Bullets.4of31.Download.24x36.pdf",
            "cta": true,
            "body": [
                {"p": "Gun silencers put law enforcement and the public at grave risk. The deregulation of these devices would only increase that risk by providing greater accessibility to criminals. Contact your congressman or congresswoman to remove the gun silencer provision from the SHARE Act."}
            ]
        },
        "5": {
            "enabledOn": "2018-05-10T06:00:00-05:00",
            "title": "Universal Background Checks",
            "img": "bullets/bullet-05.jpg",
            "thumb": "thumbs/thumb__bullet-05.jpg",
            "tags": "HTML, CSS, Javascript, Vue",
            "poster": "posters/CST.31Bullets.5of31.Download.24x36.pdf",
            "body": [
                {"p": "Though more than 90% of the American public supports background checks for all gun sales, a loophole in federal gun laws exempts unlicensed sellers from having to perform any background check whatsoever before selling a firearm."},
                {"p": "<a href=\"https://secure.ngpvan.com/Ekal5ph2tkmZW7QpupD5Qg2\" target=\"_blank\">Sign petition</a> to demand background checks on all gun sales"}
            ]
        },
        "6": {
            "enabledOn": "2018-05-11T06:00:00-05:00",
            "title": "The Power of the NRA",
            "img": "bullets/bullet-06.jpg",
            "thumb": "thumbs/thumb__bullet-06.jpg",
            "tags": "HTML, CSS, Javascript, Vue",
            "poster": "posters/CST.31Bullets.6of31.Download.24x36.pdf",
            "body": [
                {"p": "The National Rifle Association is the single biggest player in shaping gun laws in America. The group’s influence results in lax laws and minimal regulation of guns."},
                {"p": "Text “NRA” to 50409, a service operated by Resistbot, to learn how much money the NRA has spent in funding, both for and against, your current U.S. representatives and senators."}
            ]
        },
        "7": {
            "enabledOn": "2018-05-12T06:00:00-05:00",
            "title": "Open Carry",
            "img": "bullets/bullet-07.jpg",
            "thumb": "thumbs/thumb__bullet-07.jpg",
            "tags": "HTML, CSS, Javascript, Vue",
            "poster": "posters/CST.31Bullets.7of31.Download.24x36.pdf",
            "body": [
                {"p": "The presence of guns in public can quickly escalate everyday conflicts into deadly altercations. Illinois doesn’t allow the open carry of guns, but 45 states do. Urge businesses to ban open carry of guns on their property."},
                {"p": "Sign Everytown’s “Groceries, Not Guns” <a href=\"https://act.everytown.org/sign/kroger-petition/\" target=\"_blank\">petition</a> to prohibit people from openly carrying guns in Kroger stores, the nation’s largest grocery chain."}
            ]
        },
        "8": {
            "enabledOn": "2018-05-13T06:00:00-05:00",
            "title": "Licenses",
            "img": "bullets/bullet-08.jpg",
            "thumb": "thumbs/thumb__bullet-08.jpg",
            "tags": "HTML, CSS, Javascript, Vue",
            "poster": "posters/CST.31Bullets.8of31.Download.24x36.pdf",
            "cta": true,
            "body": [
                {"p": "Gun owner licenses usually are issued or renewed only after the applicant has undergone a background check, completed a gun safety training course, and passed tests on how to safely load, fire and store a gun. Licensing laws are a simple, yet effective way to ensure guns are purchased and used by responsible Americans. Illinois has such a law, but the federal government — and many states — do not.<sup>3</sup>"},
                {"p": "Contact your representative about requiring licenses for purchasing of all arms."}
            ],
            "cite": "<sup>3</sup>Giffords: <a href=\"http://lawcenter.giffords.org/gun-laws/policy-areas/gun-owner-responsibilities/licensing/\" target=\"_blank\">http://lawcenter.giffords.org/gun-laws/policy-areas/gun-owner-responsibilities/licensing/</a>"
        },
        "9": {
            "enabledOn": "2018-05-13T06:00:00-05:00",
            "title": "Licenses",
            "img": "bullets/bullet-01.jpg",
            "thumb": "thumbs/thumb__bullet-01.jpg",
            "tags": "HTML, CSS, Javascript, Vue",
            "poster": "posters/CST.31Bullets.8of31.Download.24x36.pdf",
            "cta": true,
            "body": [
                {"p": "Gun owner licenses usually are issued or renewed only after the applicant has undergone a background check, completed a gun safety training course, and passed tests on how to safely load, fire and store a gun. Licensing laws are a simple, yet effective way to ensure guns are purchased and used by responsible Americans. Illinois has such a law, but the federal government — and many states — do not.<sup>3</sup>"},
                {"p": "Contact your representative about requiring licenses for purchasing of all arms."}
            ],
            "cite": "<sup>3</sup>Giffords: <a href=\"http://lawcenter.giffords.org/gun-laws/policy-areas/gun-owner-responsibilities/licensing/\" target=\"_blank\">http://lawcenter.giffords.org/gun-laws/policy-areas/gun-owner-responsibilities/licensing/</a>"
        },
    }
}
