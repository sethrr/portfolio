# 31 Bullets Poster Site

> Ogilvy Chicago | _version 1.0.0_<br>
> Simple Vue Application for [31bullets.org](https://31bullets.org)

## About
31 bullet points and posters to counter the negative effect that these actual bullets create.

## Requirements
* [Node Installation](https://nodejs.org/en/download/) or use **Brew**
* [Vue Installation](https://vuejs.org/v2/guide/installation.html)

## Set Up

``` bash
# clone repo
$ https://YOUR.USERNAME@git.ogilvy-digital.com/scm/ogc/31-bullets.git

# navigate to project root
$ cd SourceCode/

# install dependencies
$ npm install

# will run compilation of assets and serve with webpack dev server at localhost:8080
$ npm run dev

# build for production
$ npm run build

```

Add aliases file to your **.bash_profile**<br>
<code> source 'PATH TO REPO/DeploymentScripts/aliases.sh'</code>


## Deployment

``` bash
# build for production
$ npm run build
```
* Check-in files that are generated in /built
* Run build on Jenkins:
https://build.ogilvy-digital.com/job/Ogilvy-Chicago/job/31Bullets/

