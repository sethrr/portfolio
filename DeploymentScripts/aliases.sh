#!/usr/bin/env bash
BULLETS_BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"
BULLETS_BASE_DIR="$(readlink -f "${BULLETS_BASE_DIR}" 2> /dev/null || greadlink -f "${BULLETS_BASE_DIR}" 2> /dev/null)"
if [ $? -ne 0 ]; then
    echo "There was an issue resolving paths, it appears you do not have 'readlink -f' or 'greadlink' available. If you have brew, please install coreutils: brew install coreutils"
fi
BULLETS_SCRIPT_DIR="${BULLETS_BASE_DIR}/DeploymentScripts"

export BULLETS_BASE_DIR
export BULLETS_SCRIPT_DIR

# Notice Styling #
# use like `snotice message "Some message here."`.
# message: adds a newline before the message, intended to be a header.
# command: display the raw command about to be executed to the user.
# warning: display a warning message.
# error: display an error message.
# -: no styling, any style that does not match the aformentioned style names will default to this.
function snotice() {
    local notice_style=''
    local notice_closure=$'\033[0m'

    case "$1" in
        'message')
            notice_style=$'\n\033[32m'
            ;;
        'command')
            notice_style=$'\033[36m'
            ;;
        'warning')
            notice_style=$'\033[35m'
            ;;
        'error')
            notice_style=$'\033[1;31m'
            ;;
        *)
            notice_closure=''
            ;;
    esac

    echo "${notice_style}${*:2}${notice_closure}"
}
export -f snotice

# 31 Bullets Namespace #
function bullets() {
    if [[ -z $@ || $1 == help ]]; then
        local yellow=$'\033[1;33m'
        local cyan=$'\033[1;36m'
        local creset=$'\033[0m'
        local helptext

        helptext="$(< "${BULLETS_SCRIPT_DIR}/helptext")"
        helptext="${helptext//\$\{yellow\}/${yellow}}"
        helptext="${helptext//\$\{cyan\}/${cyan}}"
        helptext="${helptext//\$\{creset\}/${creset}}"

        echo -e "$helptext" | column -ts $'\t'
        return
    fi

    case $1 in
        cdr)
            cd "${BULLETS_BASE_DIR}"
            ;;
        cds)
            cd "${BULLETS_BASE_DIR}/SourceCode"
            ;;
        dev)
            bullets cds && npm run dev
            ;;
        deploy)
            if [[ $2 == "--yolo" ]]; then

                echo ">> PROD BUILD - npm install."
                bullets cds && npm install

                echo ">> PROD BUILD - npm run build."
                npm run build

                echo ">> PROD BUILD - RSYNC to the Production Server."
                rsync \
                    -azP \
                    --delete \
                    --exclude '.DS_Store' \
                    "${BULLETS_BASE_DIR}/SourceCode/dist/" \
                    "ogilvyproject:/var/www/vhosts/31bullets.org/httpdocs"
                echo " "

                echo ">> PROD BUILD - COMPLETE."
                return
            else
                echo ">> DRY RUN - NPM INSTALL."
                bullets cds && npm install

                echo ">> DRY RUN - BUILD."
                npm run build

                echo ">> DRY RUN - RSYNC to the Production Server."
                rsync \
                    -azP \
                    --delete \
                    --exclude '.DS_Store' \
                    --dry-run \
                    "${BULLETS_BASE_DIR}/SourceCode/dist/" \
                    "ogilvyproject:/var/www/vhosts/31bullets.org/httpdocs"
                echo " "
                echo ">> DRY RUN - COMPLETE."
                return
            fi    
            ;;
        *)
            echo "Unknown command: $1" >&2
            ;;
    esac
}

